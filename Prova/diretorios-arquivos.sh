#!/bin/bash

for diretorios_nome in "$@"
do
    mkdir "$diretorios_nome"
    echo "# $diretorios_nome" > "$diretorios_nome/README.md"
done
