#!/bin/bash


motivacao=("Esforce-se para não ser um sucesso e sim pra ser valioso" 
              "Dificil?Não se preocupe!Esse é o sinal que você está no caminho certo" 
              "Não importa quão lento você vá, contanto que você não pare" 
              "Não desista alguém se inspira em você" 
              "Abra-se para a coragem e vá atrás dos seus sonhos" 
              "Lutar sempre, vencer as vezes e desistir nunca" 
              "Sempre parece impossivel até que seja feito" 
              "100% de mim não é nada comparado a 1% do time inteiro" 
              "Pense fora da caixa" 
              "Sucesso é uma das consequencias de ter acreditado")

for i in "${motivacao[@]}"
do
    echo $i
done
